﻿using UnityEngine;
using UnityEngine.UI;
public class TextController : MonoBehaviour
{
    public Text GameText;
    public Text ItemText;
    public string item;
    public int playerState = 0;

    private void Start()
    {

    }

    public void Update()
    {
        ItemText.text = item;
        if (playerState == 0)
        {
            GameText.text = "회색으로 칠해진 텅 빈 방이다. 뒤를 보자 문이 보인다.\n\n\n *문을 연다 : A *주변을 살핀다 : B";
            if (Input.GetKeyDown("a"))
            {
                playerState = 1;
            }
            if (Input.GetKeyDown("b"))
            {
                playerState = 2;
            }
        }
        else if (playerState == 1)
        {
            if (FindItem("열쇠"))
            {
                GameText.text = "문고리 밑에 조그마한 열쇠구멍이 보인다.\n\n\n *열쇠를 사용한다 : A";
                if (Input.GetKeyDown("a"))
                {
                    playerState = 3;
                }
            }
            else
            {
                GameText.text = "잠겨있다. 힘껏 밀어보았지만 열릴기미가 보이지 않는다. 문고리 밑에 조그마한 열쇠구멍이 보인다.\n\n\n *돌아간다 : A";
                if (Input.GetKeyDown("a"))
                {
                    playerState = 0;
                }
            }
        }
        else if (playerState == 2)
        {
            if (FindItem("열쇠"))
            {
                GameText.text = "둘러봐도 보이는것은 없다.\n\n\n *돌아간다 : A";
                if (Input.GetKeyDown("a"))
                {
                    playerState = 0;
                }
            }
            else
            {
                GameText.text = "주변을 살펴보니 구석진곳에 열쇠가 보인다.\n\n\n *줍는다 : A";
                if (Input.GetKeyDown("a"))
                {
                    AddItem("열쇠");
                    playerState = 0;
                }
            }
        }
        else if (playerState == 3)
        {
            GameText.text = "탈출 성공!!\n\n\n *다시하기 : A";
            if (Input.GetKeyDown("a"))
            {
                item = "";
                playerState = 0;
            }
        }

    }

    public void InitializeGame()
    {
        item = "";
        AddItem("연필");
        AddItem("종이");
        playerState = 0;
    }

    public bool FindItem(string itemName)
    {
        string[] items;
        items = item.Split(',');
        foreach (string itemObj in items)
        {
            if (itemObj == itemName)
            {
                return true;
            }
        }
        return false;
    }

    public void AddItem(string itemName)
    {
        if (item == "")
        {
            item = itemName;
        }
        else
        {
            item = item + "," + itemName;
        }
    }

    public void UseItem(string itemName)
    {
        if (FindItem(itemName))
        {
            string[] items;
            items = item.Split(',');
            item = "";
            foreach (string itemObj in items)
            {
                if (itemObj != itemName)
                {
                    AddItem(itemObj);
                }
            }
        }
    }
}