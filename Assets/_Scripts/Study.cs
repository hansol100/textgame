﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Study : MonoBehaviour
{

    public int[] arr = new int[18] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 };

    // 초기 조건 증감
    // 
    void Start()
    {

        //0 부터 100 까지 출력 3배수가 아니고 2의 배수인 숫자를 출력
      
        int idx = 0;
        while (idx <= 100)
        {

            if ( idx%2 == 0 && idx%3 != 0 )
            {
                print(idx);
            }
                
            
            idx++;
        }
    }

}
